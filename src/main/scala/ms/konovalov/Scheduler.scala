package ms.konovalov

import java.io.PrintWriter
import java.time.Instant

import io.circe
import ms.konovalov.implicits._

import scala.io.Source

/**
  * Scheduled performance event that comes from input data
  *
  * @param band     [[String]] name of the band
  * @param priority [[Integer]] priority of the event
  * @param start    start time
  * @param finish   finish time
  */
case class ScheduledPerformance(band: String, priority: Int, start: Instant, finish: Instant)

/**
  * Performance event to visit as output
  *
  * @param band [[String]] name of the band
  * @param start start time
  * @param finish finish time
  */
case class Performance(band: String, start: Instant, finish: Instant)

/**
  * Support class that represents time slice - simple time segment from start to finish
  *
  * @param start  start time
  * @param finish finish time
  */
case class Slice(start: Instant, finish: Instant)

/**
  * Wrapper that joins [[Slice]] with [[ScheduledPerformance]]
  *
  * @param slice time slice
  * @param event event during the time segment that has higher priority, [[None]] if there is no suitable events
  */
case class SliceWithOptionalEvent(slice: Slice, event: Option[ScheduledPerformance])

/**
  * Wrapper that joins [[Slice]] with [[ScheduledPerformance]]
  *
  * @param slice time slice
  * @param event event during the time segment that has higher priority
  */
case class SliceWithEvent(slice: Slice, event: ScheduledPerformance)

/**
  * Data structure that is used to convert list of time points to list of time slices
  *
  * @see [[Scheduler.convertToSlices]]
  * @param last last added element, end of previous slice and start of the next slice
  * @param list all previous added elements
  */
case class SContainer(list: List[Slice] = List(), last: Option[Instant] = None)

/**
  * Data structure that is used to join consecutive time slices with the same event
  *
  * @see [[Scheduler.joinConsecutiveSlices]]
  * @param list list of processed slices
  * @param last last added slice
  */
case class SWEContainer(list: List[SliceWithEvent] = List(), last: Option[SliceWithEvent] = None) {

  /**
    * Add last slice to the list
    * @return full processed list of the slices
    */
  def buildList: List[SliceWithEvent] = list ++ last.toList
}

object Scheduler {

  /**
    * Take all the events and grap all time points into sorted distinct list
    *
    * @param events [[List]] of [[ScheduledPerformance]]
    * @return [[List]] of sorted and distinct time points [[Instant]]
    */
  private[konovalov] def getAllTimePoints(events: List[ScheduledPerformance]): List[Instant] =
    events.foldLeft(List[Instant]())((list, current) =>
      list ++ List(current.start, current.finish)
    ).distinct.sorted

  /**
    * Transform list of ordered time points [[List]] of [[Instant]]
    * to the list of time slices [[List]] of [[Slice]]
    * E.g. if we have time points `[1, 3, 5, 7]` then after transformation we will have `[(1,3), (3,5), (5,7)]`
    *
    * @param timePoints list of time points [[Instant]]
    * @return list of [[Slice]]
    */
  private[konovalov] def convertToSlices(timePoints: List[Instant]): List[Slice] =
    timePoints.foldLeft(SContainer())((store, current) =>
      SContainer(store.last.map(store.list :+ Slice(_, current)).getOrElse(List()), Some(current))
    ).list

  /**
    * Get time slices out of list of [[ScheduledPerformance]].
    * Let's draw the performance events on the timeline
    * |-----------event1------------------|
    * |          |--------------event2----+-----------|
    * |          |         |-event3-|     |           |
    * |          |         |        |     |           |     |-event4-|
    * |          |         |        |     |           |     |        |
    * |--slice1--|--slice2-|-slice3-|-sl4-|--slice5---|-sl6-|-slice7-|
    * So the slices will be the shortest segments between the closest ordered time points.
    * Then based on the priority of the event we can determine the event for each slice
    *
    * @param events list of performance events from input data
    * @return list of all available time slices [[Slice]]
    */
  private def slice(events: List[ScheduledPerformance]): List[Slice] =
    convertToSlices(getAllTimePoints(events))

  /**
    * Checks if there is an intersection between [[Slice]] and [[ScheduledPerformance]]
    * According to the slicing procedure slice cannot be bigger than performance
    * So there are 4 cases when intersection takes place
    *
    * 1) |-perf--|  2) |--+--perf-+--|  3) |--perf-+--|  4) |--+--perf-|
    *    |       |        |       |        |       |           |       |
    *    +-slice-+        +-slice-+        +-slice-+           +-slice-+
    *
    * @param perf performance event
    * @param slice time slice
    * @return `true` if there is an intersection, `false` otherwise
    */
  private[konovalov] def intersects(perf: ScheduledPerformance, slice: Slice): Boolean = {
    (perf.start == slice.start || perf.finish == slice.finish
      || (perf.start.isBefore(slice.start) && perf.finish.isAfter(slice.finish)))
  }

  /**
    * Joins consecutive slices with the same performance event
    *
    * @param slices list of slices with events [[SliceWithOptionalEvent]]
    * @return list of joined slices when possible
    */
  private[konovalov] def joinConsecutiveSlices(slices: List[SliceWithEvent]): List[SliceWithEvent] =
    slices.foldLeft(SWEContainer())((agg: SWEContainer, next: SliceWithEvent) => agg.last match {
      case None => SWEContainer(agg.list, Some(next))
      case Some(last) =>
        if (last.event == next.event && last.slice.finish == next.slice.start) {
          SWEContainer(agg.list, Some(SliceWithEvent(Slice(last.slice.start, next.slice.finish), next.event)))
        } else {
          SWEContainer(agg.list :+ last, Some(next))
        }
    }).buildList

  /**
    * For each slice adds the event with higher priority that took place at the same time,
    * then filter out slices with [[None]] instead of event as the slice where no events happening
    *
    * @param slices list of time slices [[Slice]]
    * @param events list of scheduled performance events from input data
    * @return list of time slices with the event of the highest priority or [[None]]
    */
  private def addEventsToSlices(slices: List[Slice], events: List[ScheduledPerformance]): List[SliceWithEvent] =
    slices.map(slice => SliceWithOptionalEvent(slice, findEventForSlice(events, slice)))
      .collect({ case SliceWithOptionalEvent(slice, Some(event)) => SliceWithEvent(slice, event) })

  /**
    * Find event with highest priority for the slice
    *
    * @param events list of all events from input
    * @param slice slice of time
    * @return
    */
  private def findEventForSlice(events: List[ScheduledPerformance], slice: Slice): Option[ScheduledPerformance] =
    events.filter(perf => intersects(perf, slice)).maxByOption(perf => perf.priority)

  /**
    * Convert [[SliceWithOptionalEvent]] to [[ScheduledPerformance]]
    *
    * @param slices list of [[SliceWithOptionalEvent]]
    * @return list of [[ScheduledPerformance]]
    */
  private def convertToPerformanceEvents(slices: List[SliceWithEvent]): List[Performance] =
    slices.map(sliceWithEvent => Performance(
      band = sliceWithEvent.event.band,
      start = sliceWithEvent.slice.start,
      finish = sliceWithEvent.slice.finish
    ))

  /**
    * Do the job: make list of time slices out of input data and then add event with the highest priority to each slice
    *
    * @param events input list of [[ScheduledPerformance]]
    * @return list of events with the highest priority
    */
  def buildSchedule(events: List[ScheduledPerformance]): List[Performance] = {
    val slices = slice(events)
    convertToPerformanceEvents(joinConsecutiveSlices(addEventsToSlices(slices, events)))
  }
}

object FileConverter {

  import io.circe.generic.auto._
  import io.circe.parser._
  import io.circe.syntax._

  /**
    * Parse file with JSON to [[List]] of [[ScheduledPerformance]]
    *
    * @param filename file name
    * @return [[Either]] of error or parsed value
    */
  def parseFile(filename: String): Either[circe.Error, List[ScheduledPerformance]] = {
    val fileContents = Source.fromFile(filename).getLines.mkString
    parseString(fileContents)
  }

  /**
    * Parse string with JSON to [[List]] of [[ScheduledPerformance]]
    *
    * @param content string json content
    * @return [[Either]] of error or parsed value
    */
  def parseString(content: String): Either[circe.Error, List[ScheduledPerformance]] =
    decode[List[ScheduledPerformance]](content)

  /**
    * Serialise to string
    *
    * @param list list of [[Performance]]
    * @return string json representation
    */
  def serialise(list: List[Performance]): String = list.asJson.spaces2

  /**
    * Serialise and write to file
    * @param filename file name
    * @param list list of [[Performance]]
    */
  def writeToFile(filename: String, list: List[Performance]): Unit =
    new PrintWriter(filename) { write(serialise(list)); close() }
}

/**
  * Extension method from scala 2.13
  */
object implicits {

  implicit class ListExt[T](list : List[T]) {

    def maxByOption[B](f: T => B)(implicit cmp: Ordering[B]): Option[T] = {
      if (list.isEmpty) None else Some(list.maxBy(f))
    }
  }
}

object Main extends App {

  if (args.length != 1) {
    println("Please provide path to the file")
    sys.exit(1)
  }
  val fileName = args(0)
  val outFileName = fileName.replaceAll("\\.[^.]*$", "") + ".optimal.json"
  FileConverter.parseFile(fileName).map(Scheduler.buildSchedule).map(FileConverter.writeToFile(outFileName, _)) match {
    case Left(error) =>
      println(s"Error occurred: ${error.getLocalizedMessage}")
      sys.exit(1)
    case Right(_) =>
      println(s"file $outFileName created")
      sys.exit(0)
  }
}


