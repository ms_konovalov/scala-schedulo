package ms.konovalov

import java.time.{Duration, Instant}

import org.scalatest.FreeSpec

class SchedulerTest extends FreeSpec {

  private val hour1 = Duration.ofHours(1)
  private val hours2 = Duration.ofHours(2)
  private val hours3 = Duration.ofHours(3)
  private val now = Instant.now()

  "getAllTimePoints" - {

    "should return empty list for empty list" in {
      assert(Scheduler.getAllTimePoints(List()).isEmpty)
    }

    "should return 2 points for list of 1 element" in {
      val start = now.minus(hour1)
      val finish = now
      val input = List(ScheduledPerformance("band", 1, start, finish))
      assert(Scheduler.getAllTimePoints(input) == List(start, finish))
    }

    "should return all distinct ordered time points" in {
      val i1 = now
      val i2 = i1.plus(hour1)
      val i3 = i1.plus(hours3)
      val input = List(
        ScheduledPerformance("band", 1, i1, i2),
        ScheduledPerformance("aaa", 3, i2, i3),
        ScheduledPerformance("bbb", 5, i1, i3),
        ScheduledPerformance("ccc", 5, i1, i3),
        ScheduledPerformance("ddd", 5, i2, i3),
      )
      assert(Scheduler.getAllTimePoints(input) == List(i1, i2, i3))
    }
  }

  "convertToSlices" - {

    "should return empty list for empty list" in {
      val input = List[Instant]()
      assert(Scheduler.convertToSlices(input).isEmpty)
    }

    "should return empty list for single element" in {
      val input = List[Instant](now)
      assert(Scheduler.convertToSlices(input).isEmpty)
    }

    "should return 1 element for 2 elements in input" in {
      val input = List[Instant](now.minus(hour1), now)
      assert(Scheduler.convertToSlices(input).size == 1)
    }
  }

  "intersects" - {
    "should return false" - {
      "case 1" in {
        val performance1 = ScheduledPerformance("", 1, now, now.plus(hour1))
        val slice1 = Slice(now.minus(hours2), now.minus(hours3))
        assert(!Scheduler.intersects(performance1, slice1))
      }

      "case 2" in {
        val performance2 = ScheduledPerformance("", 1, now.minus(hours2), now.minus(hours3))
        val slice2 = Slice(now, now.plus(hour1))
        assert(!Scheduler.intersects(performance2, slice2))
      }

      "case 3" in {
        val performance3 = ScheduledPerformance("", 1, now, now.plus(hour1))
        val slice3 = Slice(now.minus(hour1), now)
        assert(!Scheduler.intersects(performance3, slice3))
      }

      "case 4" in {
        val performance4 = ScheduledPerformance("", 1, now.minus(hour1), now)
        val slice4 = Slice(now, now.plus(hour1))
        assert(!Scheduler.intersects(performance4, slice4))
      }
    }

    "should return true" - {
      "case 1" in {
        val performance1 = ScheduledPerformance("", 1, now, now.plus(hour1))
        val slice1 = Slice(now, now.plus(hour1))
        assert(Scheduler.intersects(performance1, slice1))
      }

      "case 2" in {
        val performance2 = ScheduledPerformance("", 1, now, now.plus(hours3))
        val slice2 = Slice(now, now.plus(hour1))
        assert(Scheduler.intersects(performance2, slice2))
      }

      "case 3" in {
        val performance3 = ScheduledPerformance("", 1, now.minus(hours3), now)
        val slice3 = Slice(now.minus(hour1), now)
        assert(Scheduler.intersects(performance3, slice3))
      }

      "case 4" in {
        val performance4 = ScheduledPerformance("", 1, now.minus(hours3), now.plus(hours3))
        val slice4 = Slice(now, now.plus(hour1))
        assert(Scheduler.intersects(performance4, slice4))
      }
    }
  }

  "joinConsecutiveSlices" - {
    "should return empty list for empty list" in {
      assert(Scheduler.joinConsecutiveSlices(List()).isEmpty)
    }

    "should return single element for single element" in {
      val perf = ScheduledPerformance("", 1, now, now)
      val event = SliceWithEvent(Slice(now, now.plus(hour1)), perf)
      assert(Scheduler.joinConsecutiveSlices(List(event)).size == 1)
    }

    "should join 2 elements into 1" in {
      val perf = ScheduledPerformance("", 1, now, now)
      val event1 = SliceWithEvent(Slice(now, now.plus(hour1)), perf)
      val event2 = SliceWithEvent(Slice(now.plus(hour1), now.plus(hours2)), perf)
      assert(Scheduler.joinConsecutiveSlices(List(event1, event2)).size == 1)
    }

    "should not join 2 elements into 1" in {
      val perf1 = ScheduledPerformance("aaa", 1, now, now)
      val perf2 = ScheduledPerformance("bbb", 1, now, now)
      val event1 = SliceWithEvent(Slice(now, now.plus(hour1)), perf1)
      val event2 = SliceWithEvent(Slice(now.plus(hour1), now.plus(hours2)), perf2)
      assert(Scheduler.joinConsecutiveSlices(List(event1, event2)).size == 2)
    }
  }
}
