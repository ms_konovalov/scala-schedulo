package ms.konovalov

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, PropSpec}

class SchedulerE2ETest extends PropSpec with TableDrivenPropertyChecks with Matchers {

  val examples =
    Table(
      ("input", "expected"),
      (
        """[
          |]""".stripMargin,
        """[
          |]""".stripMargin),
      (
        """[
          |  {
          |    "band" : "Soundgarden",
          |    "priority" : 5,
          |    "start" : "1993-05-25T02:00:00Z",
          |    "finish" : "1993-05-25T02:50:00Z"
          |  }
          |]""".stripMargin,
        """[
          |  {
          |    "band" : "Soundgarden",
          |    "start" : "1993-05-25T02:00:00Z",
          |    "finish" : "1993-05-25T02:50:00Z"
          |  }
          |]""".stripMargin),
      (
        """[
          |  {
          |    "band" : "Soundgarden",
          |    "priority" : 5,
          |    "start" : "1993-05-25T02:00:00Z",
          |    "finish" : "1993-05-25T02:05:00Z"
          |  },
          |  {
          |    "band" : "Pearl Jam2",
          |    "priority" : 10,
          |    "start" : "1993-05-25T02:15:00Z",
          |    "finish" : "1993-05-25T02:38:00Z"
          |  }
          |]""".stripMargin,
        """[
          |  {
          |    "band" : "Soundgarden",
          |    "start" : "1993-05-25T02:00:00Z",
          |    "finish" : "1993-05-25T02:05:00Z"
          |  },
          |  {
          |    "band" : "Pearl Jam2",
          |    "start" : "1993-05-25T02:15:00Z",
          |    "finish" : "1993-05-25T02:38:00Z"
          |  }
          |]""".stripMargin
      ),
      (
        """|[
           |  {
           |    "band" : "Soundgarden",
           |    "priority" : 5,
           |    "start" : "1993-05-25T02:00:00Z",
           |    "finish" : "1993-05-25T02:50:00Z"
           |  },
           |  {
           |    "band" : "Pearl Jam",
           |    "priority" : 9,
           |    "start" : "1993-05-25T02:15:00Z",
           |    "finish" : "1993-05-25T02:35:00Z"
           |  },
           |  {
           |    "band" : "Pearl Jam2",
           |    "priority" : 10,
           |    "start" : "1993-05-25T02:15:00Z",
           |    "finish" : "1993-05-25T02:38:00Z"
           |  }
           |]""".stripMargin,
        """|[
           |  {
           |    "band" : "Soundgarden",
           |    "start" : "1993-05-25T02:00:00Z",
           |    "finish" : "1993-05-25T02:15:00Z"
           |  },
           |  {
           |    "band" : "Pearl Jam2",
           |    "start" : "1993-05-25T02:15:00Z",
           |    "finish" : "1993-05-25T02:38:00Z"
           |  },
           |  {
           |    "band" : "Soundgarden",
           |    "start" : "1993-05-25T02:38:00Z",
           |    "finish" : "1993-05-25T02:50:00Z"
           |  }
           |]""".stripMargin),
      (
        """[
          |  {
          |    "band" : "Soundgarden",
          |    "priority" : 5,
          |    "start" : "1993-05-25T02:00:00Z",
          |    "finish" : "1993-05-25T02:15:00Z"
          |  },
          |  {
          |    "band" : "Pearl Jam2",
          |    "priority" : 5,
          |    "start" : "1993-05-25T02:10:00Z",
          |    "finish" : "1993-05-25T02:20:00Z"
          |  }
          |]""".stripMargin,
        """[
          |  {
          |    "band" : "Soundgarden",
          |    "start" : "1993-05-25T02:00:00Z",
          |    "finish" : "1993-05-25T02:15:00Z"
          |  },
          |  {
          |    "band" : "Pearl Jam2",
          |    "start" : "1993-05-25T02:15:00Z",
          |    "finish" : "1993-05-25T02:20:00Z"
          |  }
          |]""".stripMargin
      )
    )

  property("scenario") {
    forAll(examples) { (input, expected) =>
      doTest(input, expected)
    }
  }

  private def doTest(input: String, expected: String) =
    FileConverter.parseString(input).map(Scheduler.buildSchedule).map(FileConverter.serialise) match {
      case Left(_) => throw new Exception
      case Right(res) => res should equal(expected)
    }
}
