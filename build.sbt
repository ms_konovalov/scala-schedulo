name := "Schedulo"

version := "0.1"

scalaVersion := "2.12.8"

val circeVersion = "0.11.1"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"

mainClass in (Compile, run) := Some("ms.konovalov.Main")

trapExit := false