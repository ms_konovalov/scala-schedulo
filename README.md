# Scheduling app

## Task
You receive the json document with the list of scheduled performances and a time frames.
The format of the file is:

```json
[
    {
        "band" : "Soundgarden",
        "start" : "1993-05-25T02:00:00Z",
        "finish" : "1993-05-25T02:50:00Z",
        "priority" : 5
    },
    {
        "band" : "Pearl Jam",
        "start" : "1993-05-25T02:15:00Z",
        "finish" : "1993-05-25T02:35:00Z",
        "priority" : 9
    }
]
```

Write a program that takes a list of Performance objects as input and produces optimal schedule.
1. Keep in mind it needs to be the best performance at any given time. This may mean cutting one event short to see 
a higher priority performance, then returning to the original later.
2. Travel between places is instantaneous.
3. If two performances starting at the same time have the same priority, choose either one.
4. There may also be gaps where no performances are on.


## Solution

Chosen approach: grab all the time points from all events in input data, then slice all the events via these time points.
For each slice determine the top priority event, then combine consecutive slices with the same event.
See the graph

```text
|-----------event1------------------|
|          |--------------event2----+-----------|
|          |         |-event3-|     |           |
|          |         |        |     |           |     |-event4-|
|          |         |        |     |           |     |        |
|--slice1--|--slice2-|-slice3-|-sl4-|--slice5---|-sl6-|-slice7-|
```

Here we have 4 events and we slice the timeline for 7 slices based on the time points.
Then we see that for `slice7` the only event available is `event4`, for `slice6` we don't have events at all,
and for `slice3` events `event1`, `event2` and `event3` are available.

Complexity of this solution is O(n^2) as we go over slices and for each slice we scan through all the events to 
find one with the top priority.

## Limitations

1. Input file is not closed properly, but it is not necessary as jvm terminates shortly
2. No input validation of data, e.g. there is no check that start date is before the finish date

## Running

Main class to run is `ms.konovalov.Main` and provide 1 argument - path to the input file

Also it can be run with sbt

```commandline
%sbt package

[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from .../.sbt/1.0/plugins
[info] Loading project definition from .../project
[info] Loading settings for project scala-schedulo from build.sbt ...
[info] Set current project to Schedulo (in build file:.../scala-schedulo/)
[success] Total time: 1 s, completed 15 May 2019, 4:18:44 pm
```

```commandline
%sbt "run <path_to_file>"

[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from .../.sbt/1.0/plugins
[info] Loading project definition from .../project
[info] Loading settings for project scala-schedulo from build.sbt ...
[info] Set current project to Schedulo (in build file:.../scala-schedulo/)
[info] Running ms.konovalov.Main <path_to_file>
file <path_to_file>.optimal.json created
```
